CREATE USER uworld@localhost IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON world.* TO uworld@localhost;
FLUSH PRIVILEGES;