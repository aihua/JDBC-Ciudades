package com.github.bugtamer.models;

public class Language {

	// ATTRIBUTES

	private String  name;
	private boolean isOfficial;
	private double  percentage;



	// INSTANTIATION

	public Language() { }

	public Language(String name, boolean isOfficial, double percentage) {
		super();
		this.name = name;
		this.isOfficial = isOfficial;
		this.percentage = percentage;
	}



	// GETTINGS

	public String getName() {
		return name;
	}

	public boolean isOfficial() {
		return isOfficial;
	}

	public double getPercentage() {
		return percentage;
	}



	// SETTINGS

	public void setName(String name) {
		this.name = name;
	}

	public void setOfficial(boolean isOfficial) {
		this.isOfficial = isOfficial;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}



	// OVERRIDED METHODS (Object)

	@Override
	public String toString() {
		return String.format("n=%s, o=%b, p=%f", name, isOfficial, percentage);
	}

}
