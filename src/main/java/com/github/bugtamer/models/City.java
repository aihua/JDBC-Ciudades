package com.github.bugtamer.models;

public class City {

	// ATTRIBUTES

	private int     id;
	private String  name;
	private Country country;
	private String  district;
	private int     population;



	// GETTINGS

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Country getCountry() {
		return country;
	}

	public String getDistrict() {
		return district;
	}

	public int getPopulation() {
		return population;
	}



	// SETTINGS

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public void setPopulation(int population) {
		this.population = population;
	}



	// OVERRIDED METHODS (Object)

	@Override
	public String toString() {
		return String.format("id=%d, n=%s, c=%s, d=%s, p=%d",
				id, name, country.toString(), district, population);
	}

}
