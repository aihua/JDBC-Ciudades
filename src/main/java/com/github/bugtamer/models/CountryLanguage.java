package com.github.bugtamer.models;

public class CountryLanguage {

	// ATTRIBUTES

	private String  countryCode;
	private String  language;
	private boolean isOfficial;
	private double  percentage;



	// GETTINGS

	public String getCountryCode() {
		return countryCode;
	}

	public String getLanguage() {
		return language;
	}

	public boolean isOfficial() {
		return isOfficial;
	}

	public double getPercentage() {
		return percentage;
	}



	// SETTINGS

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setOfficial(boolean isOfficial) {
		this.isOfficial = isOfficial;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}



	// OVERRIDED METHODS (Object)

	@Override
	public String toString() {
		return String.format("cc=%s, l=%s, o=%b, p=%f", countryCode, language, isOfficial, percentage);
	}

}
