package com.github.bugtamer.models;

import java.util.List;

public class Country {

	// ATTRIBUTES

	private String code;
	private String name;
	private List<Language> languages;



	// GETTINGS

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public List<Language> getLanguages() {
		return languages;
	}



	// SETTINGS

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}



	// OVERRIDED METHODS (Object)

	@Override
	public String toString() {
		return String.format("c=%s, n=%s, l=%d", code, name, languages.size());
	}

}
