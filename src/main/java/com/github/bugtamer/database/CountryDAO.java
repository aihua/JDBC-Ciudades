package com.github.bugtamer.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.github.bugtamer.models.Country;
import com.github.bugtamer.models.Language;


public class CountryDAO extends DAO {


	// ATTRIBUTES

	private static CountryDAO singletonCityDAO;



	// INSTANTIATION

	private CountryDAO() throws Exception {
		super();
	}


	public static CountryDAO getInstance() throws Exception {
		return (singletonCityDAO == null) ? new CountryDAO() : singletonCityDAO;
	}



	// SERVICES

	public List<Country> getList() {
		String query = "SELECT * FROM country ORDER BY Code ASC;";
		List<Country> cities = new ArrayList<>();
		try {
			Connection conn = DriverManager.getConnection(URL);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			Country country;
			while (rs.next()) {
				country = new Country();
				country.setName(rs.getString("Name"));
				country.setCode(rs.getString("Code"));
				country.setLanguages(new ArrayList<>()); // XXX
				cities.add(country);
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cities;
	}


	public List<Country> getCountryWithLanguagesList() {
		StringBuilder query = new StringBuilder();
		query.append("SELECT c.Name, c.Code, cl.Language, cl.IsOfficial, cl.Percentage ");
		query.append("FROM country c LEFT JOIN countrylanguage cl ON cl.countryCode = c.code ");
		query.append("ORDER BY c.code;");
		System.out.println("getCountryWithLanguagesList()=" + query.toString());
		List<Country> cities = new ArrayList<>();
		try {
			Connection conn = DriverManager.getConnection(URL);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query.toString()); // Name * Code * Language * IsOfficial * Percentage
			Country country = null;
			String currentCountryCode = null;
			while (rs.next()) {
				boolean isNewCountryCode = (currentCountryCode == null) ||
						! currentCountryCode.equals(rs.getString("Code"));

				if ((country == null)  || isNewCountryCode) {
					currentCountryCode = rs.getString("Code");
					country = new Country();
					country.setName(rs.getString("Name"));
					country.setCode(currentCountryCode);
					country.setLanguages(new ArrayList<>());
					cities.add(country);
				}
				Language lang = new Language();
				lang.setName(rs.getString("Language"));
				lang.setOfficial( parseIsOfficial(rs.getString("IsOfficial")) );
				lang.setPercentage(parsePercentage(rs.getString("Percentage")));
				country.getLanguages().add(lang);
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cities;
	}


	public Country getCountryByCode(String code) {
		String query = "SELECT * FROM country WHERE Code = ?;";
		List<Country> cities = new ArrayList<>();
		try {
			Connection conn = DriverManager.getConnection(URL);
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, code);
			ResultSet rs = stmt.executeQuery();
			Country country;
			while (rs.next()) {
				country = new Country();
				country.setName(rs.getString("Name"));
				country.setCode(rs.getString("Code"));
				country.setLanguages(new ArrayList<>()); // XXX
				cities.add(country);
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cities.get(0);
	}


	private boolean parseIsOfficial(String isOfficial) {
		boolean bIsOfficial = false;
		if (isOfficial == null) {
			System.out.println("parseIsOfficial()='" + isOfficial + "'"); // FIX
		} else {
			bIsOfficial = isOfficial.equals("T") ? true : false;
		}
		return bIsOfficial;
	}


	private double parsePercentage(String percentage) {
		double per = -0.0;
		if (percentage == null) {
			System.out.println("parsePercentage()='" + percentage + "'"); // FIX
		} else {
			per = Double.parseDouble(percentage);
		}
		return per;
	}

}
