package com.github.bugtamer.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.github.bugtamer.models.City;
import com.github.bugtamer.models.Country;


public class CityCountryDAO extends DAO {


	// ATTRIBUTES

	private static CityCountryDAO singletonCityCountryDAO;



	// INSTANTIATION

	private CityCountryDAO() throws Exception {
		super();
	}


	public static CityCountryDAO getInstance() throws Exception {
		return (singletonCityCountryDAO == null) ? new CityCountryDAO() : singletonCityCountryDAO;
	}



	// SERVICES

	public int addCityAndItsCountry(City city) throws SQLException {
		int index = DAO.NO_ID;
		if ((city != null)  &&  (city.getCountry() != null)) {
			Connection conn = null;
			try {
				conn = DriverManager.getConnection(URL);
				conn.setAutoCommit(false);
				addCountry(city.getCountry(), conn);
				index = addCity(city, conn);
				conn.commit();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				conn.rollback();
			}
		}
		return index;
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private int getIndex(Statement st) throws SQLException {
		int index = DAO.NO_ID;
		ResultSet rs = st.getGeneratedKeys();
		while(rs.next()) {
			index = rs.getInt(1);
		}
		return index;
	}


	private int addCity(City city, Connection conn) throws SQLException {
		int index = DAO.NO_ID;
		String queryCity = "INSERT INTO city (Name, CountryCode, District, Population) VALUES (?, ?, ?, ?);";
		PreparedStatement stmt = conn.prepareStatement(queryCity);
		stmt.setString(1, city.getName());
		stmt.setString(2, city.getCountry().getCode());
		stmt.setString(3, city.getDistrict());
		stmt.setInt   (4, city.getPopulation());
		System.out.println("addCity query=" + stmt.toString());
		stmt.executeUpdate();
		index = getIndex(stmt);
		stmt.close();
		return index;
	}


	private void addCountry(Country country, Connection conn) throws SQLException {
		StringBuilder queryCountry = new StringBuilder();
		queryCountry.append("INSERT INTO country ");
		queryCountry.append("(Name, Code, ");
		queryCountry.append("Continent, Region, SurfaceArea, Population, LocalName, GovernmentForm, Code2) ");
		queryCountry.append("VALUES (?, ?, ");
		queryCountry.append("?, ?, ?, ?, ?, ?, ?);");
		PreparedStatement stmt = conn.prepareStatement(queryCountry.toString());
		stmt.setString(1, country.getName());
		stmt.setString(2, country.getCode());
		stmt.setString(3, "Europe");
		stmt.setString(4, "Fake region");
		stmt.setFloat (5, 12345.5F);
		stmt.setInt   (6, 1000);
		stmt.setString(7, "Fake local name");
		stmt.setString(8, "Fake government form");
		stmt.setString(9, "FK");
		System.out.println("addCountry query=" + stmt.toString());
		stmt.executeUpdate();
		stmt.close();
	}

}
