package com.github.bugtamer.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.bugtamer.database.CityDAO;
import com.github.bugtamer.database.CountryDAO;
import com.github.bugtamer.database.DAO;
import com.github.bugtamer.models.City;
import com.github.bugtamer.models.Country;

@WebServlet("/newCity")
public class NewCityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("path", request.getServletPath());
		request.getRequestDispatcher("newCity.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name        = request.getParameter("name");
		String countryCode = request.getParameter("countryCode");
		String district    = request.getParameter("district");
		String population  = request.getParameter("population");

		int iPopulation = Integer.parseInt(population);

		City city = new City();
		city.setName(name);
		city.setCountry(getCountry(countryCode));
		city.setDistrict(district);
		city.setPopulation(iPopulation);

		int id = addCity(city);

		if (id == DAO.NO_ID) {
			request.setAttribute("error", "ERROR: comprueba los datos de la ciudad.");
			request.setAttribute("city", city);
			doGet(request, response);
		} else {
			response.sendRedirect("cities");
		}
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private int addCity(City city) {
		int id = DAO.NO_ID;
		try {
			id = CityDAO.getInstance().add(city);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("id=" + id);
		return id;
	}


	private Country getCountry(String country) {
		Country c = null;
		try {
			c = CountryDAO.getInstance().getCountryByCode(country);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c;
	}

}
