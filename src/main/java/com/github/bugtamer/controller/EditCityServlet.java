package com.github.bugtamer.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.bugtamer.database.CityDAO;
import com.github.bugtamer.database.CountryDAO;
import com.github.bugtamer.database.DAO;
import com.github.bugtamer.models.City;
import com.github.bugtamer.models.Country;

@WebServlet("/editCity")
public class EditCityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id");
		City city = getCity(id);
		if (city != null) {
			request.setAttribute("city", city);
		}
		request.setAttribute("path",        request.getServletPath());
		request.getRequestDispatcher("editCity.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id          = request.getParameter("cid");
		String name        = request.getParameter("name");
		String countryCode = request.getParameter("countryCode");
		String district    = request.getParameter("district");
		String population  = request.getParameter("population");

		int iId         = (id == null) ? DAO.NO_ID : Integer.parseInt(id);
		int iPopulation = Integer.parseInt(population);

		if (iId != DAO.NO_ID) {
			City city = new City();
			city.setId(iId);
			city.setName(name);
			city.setCountry(getCountry(countryCode));
			city.setDistrict(district);
			city.setPopulation(iPopulation);
			update(city);
		}

		response.sendRedirect("cities");
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private City getCity(String id) {
		City city = null;
		try {
			city = CityDAO.getInstance().getCityById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return city;
	}


	private Country getCountry(String country) {
		Country c = null;
		try {
			c = CountryDAO.getInstance().getCountryByCode(country);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c;
	}


	private int update(City city) {
		int rowCount = 0;
		try {
			rowCount = CityDAO.getInstance().update(city);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("rowCount=" + rowCount);
		return rowCount;
	}

}
