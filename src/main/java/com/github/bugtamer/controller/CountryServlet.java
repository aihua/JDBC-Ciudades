package com.github.bugtamer.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.bugtamer.database.CountryDAO;
import com.github.bugtamer.models.Country;

@WebServlet("/countries")
public class CountryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String query = "SELECT * FROM country";
		List<Country> countries = getCountries(query);
		request.setAttribute("path",        request.getServletPath());
		request.setAttribute("countries", countries);
		request.getRequestDispatcher("countries.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private List<Country> getCountries(String query) {
		List<Country> countries = null;
		try {
			countries = CountryDAO.getInstance().getCountryWithLanguagesList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return countries;
	}


}
