package com.github.bugtamer.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.bugtamer.database.CityDAO;

@WebServlet("/removeCity")
public class RemoveCityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("cid");
		String country = request.getParameter("country");
		boolean hasError = removeCity(id);
		if (hasError) {
			request.setAttribute("error", "ERROR: Deletion failed");
		}
		String countryCodeFilterQueryParam = (country == null) ? "" : "?country=" + country;
		response.sendRedirect("cities" + countryCodeFilterQueryParam);
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private boolean removeCity(String id) {
		boolean hasError = false;
		try {
			hasError = CityDAO.getInstance().remove(id);
		} catch (Exception e) {
			hasError = true;
			e.printStackTrace();
		}
		return hasError;
	}

}
