package com.github.bugtamer.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.bugtamer.database.CityCountryDAO;
import com.github.bugtamer.database.DAO;
import com.github.bugtamer.models.City;
import com.github.bugtamer.models.Country;

@WebServlet("/newCityCountry")
public class NewCityCountryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("path", request.getServletPath());
		request.getRequestDispatcher("newCityCountry.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name        = request.getParameter("name");
		String countryCode = request.getParameter("countryCode");
		String countryName = request.getParameter("countryName");
		String district    = request.getParameter("district");
		String population  = request.getParameter("population");

		int iPopulation = Integer.parseInt(population);

		Country country = new Country();
		country.setName(countryName);
		country.setCode(countryCode);

		City city = new City();
		city.setName(name);
		city.setCountry(country);
		city.setDistrict(district);
		city.setPopulation(iPopulation);

		int id = addCityAndItsCountry(city);

		if (id == DAO.NO_ID) {
			request.setAttribute("error", "ERROR: check city and country data.");
			request.setAttribute("city", city);
			doGet(request, response);
		} else {
			response.sendRedirect("editCity?id=" + id);
		}
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private int addCityAndItsCountry(City city) {
		int id = DAO.NO_ID;
		try {
			id = CityCountryDAO.getInstance().addCityAndItsCountry(city);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("...CAUSE BY:");
			e.getCause().printStackTrace();
		}
		System.out.println("id=" + id);
		return id;
	}

}
