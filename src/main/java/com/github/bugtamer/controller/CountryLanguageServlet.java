package com.github.bugtamer.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.bugtamer.database.CountryLanguageDAO;
import com.github.bugtamer.models.CountryLanguage;

@WebServlet("/languages")
public class CountryLanguageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String defaultQuery;
		defaultQuery  = "SELECT *\r\nFROM countrylanguage\r\n";
		defaultQuery += "WHERE Language LIKE 'a%'\r\nORDER BY Language ASC";

		List<CountryLanguage> countryLanguages = getCountryLanguages();

		request.setAttribute("path",      request.getServletPath());
		request.setAttribute("query",     defaultQuery);
		request.setAttribute("languages", countryLanguages);
		request.getRequestDispatcher("languages.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String query = request.getParameter("query");
		List<CountryLanguage> countryLanguages = getCountryLanguages(query);

		request.setAttribute("query",     query);
		request.setAttribute("languages", countryLanguages);
		request.getRequestDispatcher("languages.jsp").forward(request, response);
	}



	// LOW LEVEL IMPLEMENTATION DETAILS

	private List<CountryLanguage> getCountryLanguages() {
		return getCountryLanguages("");
	}


	private List<CountryLanguage> getCountryLanguages(String query) {
		List<CountryLanguage> countryLanguages = null;
		try {
			if (query.isEmpty()) {
				countryLanguages = CountryLanguageDAO.getInstance().getList();
			} else {
				countryLanguages = CountryLanguageDAO.getInstance().getList(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return countryLanguages;
	}

}
