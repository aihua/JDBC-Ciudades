<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.github.bugtamer.models.CountryLanguage" %>
<%@ taglib prefix="w" tagdir="/WEB-INF/tags" %>
<html>
<head>
	<link rel="stylesheet" href="./css/world.css">
</head>
<body>
	<div id="container">
		<h1>World</h1>
		<h2>Languages</h2>
		<form action="./languages" method="POST">
			<textarea id="query" name="query" cols="50" rows="5">${query}</textarea>
			<br>
			<button>Sumbit</button>
			<br>
		</form>
		<w:nav></w:nav>
		<ul>
			<c:forEach var="lang" items="${languages}">
			<li>
				${lang.language}
				<ul>
					<li>${lang.countryCode}</li>
					<li>Is official? ${lang.official}</li>
					<li>${lang.percentage}</li>
				</ul>
			</li>
			</c:forEach>
		</ul>
	</div>
</body>
</html>
