<%@ page import="com.github.bugtamer.models.City" %>
<html>
<head>
	<link rel="stylesheet" href="./css/world.css">
</head>
<body>
	<div id="container">
		<h1>World</h1>
		<h2>New City</h2>

		<form action="./newCity" method="POST">
			<input name="name"        type="text"   value="Fake name"      placeholder="name" /><br>
			<input name="countryCode" type="text"   value="AND"            placeholder="countryCode" /><br>
			<input name="district"    type="text"   value="Fake district" placeholder="district" /><br>
			<input name="population"  type="number" value="100"            placeholder="population"  /><br>
			<button>New</button>
			<br>
			<a href="./cities">go to Cities</a>
		</form>
		<p class="error">${error}</p>
	</div>
</body>
</html>
