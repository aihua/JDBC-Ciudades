<%@ page import="com.github.bugtamer.models.City" %>
<html>
<head>
	<link rel="stylesheet" href="./css/world.css">
</head>
<body>
	<div id="container">
		<h1>World</h1>
		<h2>Edit City</h2>
		<h3>${city.name}</h3>
		<h4>id=${city.id}</h4>

		<form action="./editCity" method="POST">
			<input name="cid"         type="hidden" placeholder="id"          value="${city.id}" /><br>
			<input name="name"        type="text"   placeholder="name"        value="${city.name}" /><br>
			<input name="countryCode" type="text"   placeholder="countryCode" value="${city.country.code}" /><br>
			<input name="district"    type="text"   placeholder="district"    value="${city.district}" /><br>
			<input name="population"  type="number" placeholder="population"  value="${city.population}" /><br>
			<button>Update</button>
			<br>
			<a href="./cities">go to Cities</a>
		</form>
	</div>
</body>
</html>
