<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.github.bugtamer.models.Language" %>
<%@ page import="com.github.bugtamer.models.CountryLanguage" %>
<%@ page import="com.github.bugtamer.models.Country" %>
<%@ taglib prefix="w" tagdir="/WEB-INF/tags" %>
<html>
<head>
	<link rel="stylesheet" href="./css/world.css">
</head>
<body>
	<div id="container">
		<h1>World</h1>
		<h2>Countries</h2>
		<w:nav/>
		<ul>
			<c:forEach var="country" items="${countries}">
			<li>${country.name}
				<ul>
					<li>Code: ${country.code}</li>
					<li>Lang: ${country.languages.size()}
						<ol>
							<c:forEach var="lang" items="${country.languages}">
							<li>${lang.name}
								<ul>
									<li>${lang.official ? "Official" : "NO official"}</li>
									<li>${lang.percentage}%</li>
								</ul>
							</li>
							</c:forEach>
						</ol>
					</li>
				</ul>
			</li>
			</c:forEach>
		</ul>
	</div>
</body>
</html>
