<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<form action="./cities" method="GET">
			<select name="country">
				<option value="" selected="selected" disabled="disabled"></option>
				<c:forEach var="country" items="${countries}">
				<option value="${country.code}">${country.name}</option>
				</c:forEach>
			</select>
			<button>Filter</button>
		</form>
