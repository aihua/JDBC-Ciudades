<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<ul>
			<c:forEach var="city" items="${cities}">
			<li>
				<a href="./editCity?id=${city.id}">${city.name}</a>
				<ul>
					<li>${city.id}</li>
					<li>${city.country.code}</li>
					<li>${city.district}</li>
					<li>${city.population}</li>
					<li>
						<a href="./removeCity?cid=${city.id}${(countryCodeFilter == null) ? "" : String.format("&country=%s", countryCodeFilter)}">DELETE</a>
					</li>
				</ul>
			</li>
			</c:forEach>
		</ul>
