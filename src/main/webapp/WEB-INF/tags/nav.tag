		<% String path = (String) request.getAttribute("path"); %>
		<br>
		<% if (! path.equals("/cities")) { %>
		<a href="./cities">List cities</a>
		<br>
		<% } %>
		<% if (! path.equals("/customCityQuery")) { %>
		<a href="./customCityQuery">Custom city query</a>
		<br>
		<% } %>
		<% if (! path.equals("/newCity")) { %>
		<a href="./newCity">Add city</a>
		<br>
		<% } %>
		<% if (! path.equals("/countries")) { %>
		<a href="./countries">Countries</a>
		<br>
		<% } %>
		<% if (! path.equals("/languages")) { %>
		<a href="./languages">Languages</a>
		<br>
		<% } %>
		<% if (! path.equals("/newCityCountry")) { %>
		<a href="./newCityCountry">new City-Country</a>
		<br>
		<% } %>
		<br>
